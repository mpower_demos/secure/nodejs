FROM node:lts

ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION
ARG BUILD_BRANCH
ARG APP_DIR='/opt/nodejs/'

LABEL MAINTAINER="mpower@gitlab.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=$BUILD_DATE \
org.label-schema.name="mpower/nodejs" \
org.label-schema.description="mpower Node Demo" \
org.label-schema.vcs-url="https://gitlab.com/gitlab-gold/mpower/nodejs" \
org.label-schema.vcs-ref=$VCS_REF \
org.label-schema.version=$BUILD_VERSION \
org.label-schema.vendor="Maxwell Power"

COPY . "${APP_DIR}"
WORKDIR "${APP_DIR}"

RUN npm install

ENTRYPOINT ["npm"]
CMD ["start"]

EXPOSE 8080
