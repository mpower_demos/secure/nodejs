const port = process.env.PORT || 8080;

// Import Required Modules
const path = require('path');
const compression = require('compression');
const express = require('express');

// Initialise the app
const app = express();
app.use(compression())

// Default URL
app.all('/', (req, res) => res.sendFile(path.join(__dirname, 'html', 'index.html')));

// Healthcheck
app.use('/healthcheck', require('express-healthcheck')());

// Everything else
app.all('/*', function (req, res) {
    res.status(404).json({status: 'ERROR', message: 'Endpoint Not Found'})
});

startService()
    .then(r => console.log('Running API Service on ' + port))
    .catch(r => console.ERROR('Failed to Start API Service'));

async function startService() {
    await app.listen(port);
}
