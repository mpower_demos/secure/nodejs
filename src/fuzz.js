const mode = process.env.FUZZ_MODE;

function fuzz(buf) {
    const string = buf.toString();
    if (string.length === 3) {
        console.log(string);
        if (string[0] === 'f' && string[1] === 'u' && string[2] === 'z') {
            if (mode === 'success') {
                process.exit(0)
            } else {
                throw Error("Fuzz Example Exception")
            }
        }
    }
}

module.exports = {
    fuzz
};
